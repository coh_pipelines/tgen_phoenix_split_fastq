ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_rsync:3.1.3 AS opt_rsync

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:3.11.3

RUN apk update && \
    apk add bash

RUN apk update && \
    apk add coreutils

COPY --from=opt_rsync /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/
